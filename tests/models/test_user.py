from tests.utils import login


def test_available_sl_domains(flask_client):
    user = login(flask_client)

    assert set(user.available_sl_domains()) == {"d1.test", "d2.test", "sl.local"}
